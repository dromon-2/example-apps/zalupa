# frozen_string_literal: true

require_relative 'lib/zalupa/version'

Gem::Specification.new do |spec|
  spec.name          = 'zalupa'
  spec.version       = Zalupa::VERSION
  spec.authors       = ['Alexander Smirnov']
  spec.email         = ['jelf.personal@zoho.eu']

  spec.summary       = 'Zalupa calculator'
  spec.description   = 'Polnaya zalupa'
  spec.homepage      = 'http://zalupa.com'
  spec.license       = 'MIT'
  spec.required_ruby_version = '>= 3.0.0'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
