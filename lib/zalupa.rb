# frozen_string_literal: true

require_relative 'zalupa/version'
require_relative 'zalupa/calculator'

module Zalupa
  class Error < StandardError; end
  # Your code goes here...

  def self.calculate(input)
    Calculator.new(input).calculate
  end
end
