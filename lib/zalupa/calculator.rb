# frozen_string_literal: true

module Zalupa
  class Calculator
    def initialize(input)
      a, op, b = input.split

      @a = a.to_f
      @b = b.to_f
      @op = op.to_sym
    end

    def calculate
      @a.send(@op, @b)
    end
  end
end
